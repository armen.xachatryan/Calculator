package com.example.android.calculator;

import java.util.Stack;
import java.util.StringTokenizer;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

    private TextView display;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        display = (TextView) findViewById(R.id.infoTextView);

        findViewById(R.id.button0).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append("0");
            }
        });
        findViewById(R.id.button1).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append("1");
            }
        });
        findViewById(R.id.button2).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append("2");
            }
        });
        findViewById(R.id.button3).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append("3");
            }
        });
        findViewById(R.id.button4).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append("4");
            }
        });
        findViewById(R.id.button5).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append("5");
            }
        });
        findViewById(R.id.button6).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append("6");
            }
        });
        findViewById(R.id.button7).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append("7");
            }
        });
        findViewById(R.id.button8).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append("8");
            }
        });
        findViewById(R.id.button9).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append("9");
            }
        });

        findViewById(R.id.buttonDot).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append(".");
            }
        });
        findViewById(R.id.buttonl).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append("(");
            }
        });
        findViewById(R.id.buttonr).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append(")");
            }
        });
        findViewById(R.id.buttonToggle).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append("-");
            }
        });
        findViewById(R.id.buttonPlus).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append("+");
            }
        });
        findViewById(R.id.buttonMinus).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append("-");
            }
        });
        findViewById(R.id.buttonMul).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append("*");
            }
        });
        findViewById(R.id.buttonDivide).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display.append("/");
            }
        });

        findViewById(R.id.buttonEquals).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = calculate(display.getText().toString());
                display.setText(s);
            }
        });

        findViewById(R.id.buttonClear).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                display = (TextView) findViewById(R.id.infoTextView);
                display.setText("");
            }
        });

    }

    private String calculate(String str) {
        String s;
        if (str.indexOf('(') != -1 && str.indexOf(')') != -1) {
            int lScope = str.indexOf('(');
            int rScope = str.lastIndexOf(')');

            if (lScope > rScope) {
                s = calculate(str.substring(lScope + 1, str.length()));
                String s1 = calculate(str.substring(0, rScope));
                str = s1 + str.substring(rScope + 1,lScope) + s;
            } else {
                s = calculate(str.substring(lScope + 1, rScope));
                str = str.substring(0, lScope) + s + str.substring(rScope + 1, str.length());
            }
        }

        float result = 0;
        Stack<Float> st = new Stack<Float>();
        StringTokenizer numbers = new StringTokenizer(str, "+-/* ");
        StringTokenizer operators = new StringTokenizer(str, "1234567890(). ");

        st.push(Float.parseFloat(numbers.nextToken()));
        while (numbers.hasMoreTokens()) {
            char op = operators.nextToken().charAt(0);
            final char minus = '-';
            String num = numbers.nextToken();
            float a;

            if (str.charAt(0) == minus) {
                a = st.pop();
                a = -a;
                st.push(a);
                op = operators.nextToken().charAt(0);
            }

            if (str.charAt(0) == '*' || str.charAt(0) == '/')
                return "Error";


            switch (op) {
                case '*':
                    a = st.pop();
                    st.push(a * Float.parseFloat(num));
                    break;
                case '/':
                    a = st.pop();
                    if (Float.parseFloat(num) != 0)
                        st.push(a / Float.parseFloat(num));
                    else
                        return "Error";
                    break;
                case '+':
                    st.push(Float.parseFloat(num));
                    break;
                case '-':
                    st.push((-1) * Float.parseFloat(num));
                    break;
            }
        }

        while (!st.isEmpty()) {
            result += st.pop();
        }
        return Float.toString(result);
    }
}